// Libs
import { it, describe } from "mocha";
import { expect } from "chai";

// Modules
import { parseDTOFromJson } from "./parse-dto-from-json.module";

describe("parseDtoFromJson", () => {
  it("Should be able to parse object using schema", () => {
    parseDTOFromJson([
      {
        name: "hello",
        properties: [
          { name: "id", type: "number" },
          { name: "name", type: "string" },
          { name: "description", type: "string", optional: true },
          { name: "world", type: "world" },
          { name: "worlds", type: "world[]" },
          { name: "multi", type: "string | number" },
          { name: "multi2", type: "string | number[]" },
          { name: "multi3", type: "string | world" },
        ]
      },
      {
        name: "world",
        properties: [
          { name: "id", type: "number" },
          { name: "tags", type: "string[]", optional: true }
        ]
      }
    ], {
      id: 1,
      name: "myHelloObject",
      world: {
        id: 2,
        tags: ["a", "b", ""]
      },
      worlds: [{
        id: 3
      }],
      multi: 1,
      multi2: [1, 2, 3],
      multi3: { id: 4 }
    }, "hello");
  })
});