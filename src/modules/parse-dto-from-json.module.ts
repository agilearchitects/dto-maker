import { jsonType, ISchemaDTO, IPropertyDTO } from "../dto/schema.dto";
import { IDictionaryDTO } from "../dto/dictionary.dto";

export const parseDTOFromJson = <T>(
  schemas: ISchemaDTO[],
  object: IDictionaryDTO<jsonType>,
  schemaName: string
): T => {
  // Make sure schema for parsing exists in schema list
  const schema: ISchemaDTO | undefined = schemas.find((schema: ISchemaDTO) => schema.name === schemaName);
  if (schema === undefined) {
    throw new Error(`Unable to find schema "${schemaName}"`);
  }

  // Prepare return object
  return schema.properties.reduce((previousValue: any, property: IPropertyDTO) => {
    const objectPropertyValue: jsonType = object[property.name];

    if (!(property.name in object) && (property.optional === undefined || property.optional === false)) {
      // Throws error if property don't exist but is required to do so
      throw new Error(`Property "${property.name}" could not be found in object when parsing from schema "${schema.name}"`);
    } else if (!(property.name in object)) {
      // Property is optional and don't exists in object
      return { ...previousValue };
    }

    return {
      ...previousValue,
      [property.name]: (() => {
        // Set array of all property types
        const propertyTypes = property.type.split("|").map((type: string) => type.trim());
        propertyTypesIteration:
        for (let a = 0; a < propertyTypes.length; a++) {
          if (["string", "number", "boolean", "null"].includes(propertyTypes[a])) {
            if (typeof objectPropertyValue !== propertyTypes[a] || ((propertyTypes[a] as any) === "null" && objectPropertyValue !== null)) {
              if (a === propertyTypes.length - 1) {
                // Throw error if end of iteration
                throw new Error(`Property "${property.name}" of schema "${schema.name}" could not be parsed since its the wrong data type. Type "${property.type}" was expected, Type "${typeof objectPropertyValue}" was provided`);
              } else {
                // Skip rest of loop
                continue;
              }
            }
            // Validation passed. Return value
            return objectPropertyValue;
          } else {
            // Is property type array (of something)?
            const match = propertyTypes[a].match(/^(.+)\[\]$/);
            if (match !== null) {
              // Check that object property is an array
              if (!(objectPropertyValue instanceof Array)) {
                if (a === propertyTypes.length - 1) {
                  // Throw error if end of iteration
                  throw new Error(`Property "${property.name}" expected object to be of an Array. Type "${typeof objectPropertyValue}" was provided`);
                } else {
                  // Skip rest of loop
                  continue;
                }
              }

              // Prepare empty array for return object
              const returnObject: any = [];

              // Get the type to expect for each array object
              const arrayType: string = match[1];

              // Iterate over each array value of object property value
              for (const arrayObject of objectPropertyValue) {
                // Simple type expected
                if (["string", "number", "boolean", "null"].includes(arrayType)) {
                  // Check that array object is expected array type
                  if (typeof arrayObject !== arrayType || ((arrayType as any) === "null" && arrayObject !== null)) {
                    if (a === propertyTypes.length - 1) {
                      // Throw error if end of iteration
                      throw new Error(`Property "${property.name}" of schema "${schema.name}" could not be parsed since its the wrong data type. Type "${arrayType}" was expected, Type "${typeof arrayObject}" was provided`);
                    } else {
                      // Skip rest of parent loop
                      continue propertyTypesIteration;
                    }
                  }
                  // Pushes array object to return object array
                  returnObject.push(arrayObject);
                } else {
                  // Property expected is not simple

                  /* Type checking of object before parsing as another schema using
                  this.parseFromJson */
                  if (typeof arrayObject === "string" || typeof arrayObject === "number" || typeof arrayObject === "boolean" || arrayObject === null || (arrayObject instanceof Array)) {
                    if (a === propertyTypes.length - 1) {
                      // Throw error if end of iteration
                      throw new Error(`Property "${property.name}" of schema "${schema.name}" expected each array instance to be of type "${arrayType}" but "${typeof arrayObject}" was provided`);
                    } else {
                      // Skip rest of parent loop
                      continue propertyTypesIteration;
                    }
                  }

                  // Parse with this.parseFromJson
                  returnObject.push(parseDTOFromJson(schemas, arrayObject, arrayType));
                }
              }
              return returnObject;
            } else {
              /* Property expected is not simple (string, number, boolean
              or null) and not an array. Try to parse it as another schema
              using this.parseFromJson*/
              if (typeof objectPropertyValue === "string" || typeof objectPropertyValue === "number" || typeof objectPropertyValue === "boolean" || objectPropertyValue === null || (objectPropertyValue instanceof Array)) {
                throw new Error(`Property "${property.name}" of schema "${schema.name}" expected object to be of type "${property.type}" but "${typeof objectPropertyValue}" was provided`);
              }
              return parseDTOFromJson(schemas, objectPropertyValue, propertyTypes[a]);
            }
          }
        }
      })()
    }
  }, {});
}