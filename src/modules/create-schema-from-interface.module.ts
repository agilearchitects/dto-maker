// Libs
import ts, { SyntaxKind, CompilerOptions, CompilerHost, Program, Diagnostic } from "typescript";

// DTO's
import { ISchemaDTO, IPropertyDTO } from "../dto/schema.dto";

export interface ITsModule {
  createProgram(rootNames: readonly string[], options: CompilerOptions, host?: CompilerHost, oldProgram?: Program, configFileParsingDiagnostics?: readonly Diagnostic[]): Program;
  SyntaxKind: typeof SyntaxKind;
}

export const createSchemaFromInterface = (
  filePath: string,
  interfaceName: string,
  tsModule: ITsModule,
): ISchemaDTO => {
  const program = tsModule.createProgram([filePath], {});
  const file = program.getSourceFile(filePath);
  if (file === undefined) {
    throw new Error(`File "${filePath}" could not be parsed`);
  }

  const myInterface: ts.InterfaceDeclaration = file.getChildAt(0).getChildren().find((child: any) => child.kind === tsModule.SyntaxKind.InterfaceDeclaration && child.name.escapedText === interfaceName) as ts.InterfaceDeclaration;

  if (myInterface === undefined) {
    throw new Error(`Interface "${interfaceName}" could not be found in file: ${filePath}`);
  }

  const match = file.text.substr(myInterface.pos, myInterface.end).match(/^\s*(?:\/\/([^\n]*)|\/\*((?:(?!\*\/).)*))/s);
  let description: string | undefined;
  if (match !== null) {
    if (match[1] !== undefined) { description = match[1].trim(); }
    else { description = match[2].trim(); }
  }
  return {
    name: myInterface.name.escapedText.toString(),
    ...(description !== undefined ? { description } : undefined),
    properties: myInterface.members.map<IPropertyDTO>((member: any) => {
      const typeChecker = program.getTypeChecker();
      const match = file.text.substr(member.pos, member.end - member.pos).match(/(?:\/\/([^\n]*)|\/\*((?:(?!\*\/).)*))/s);
      let description: string | undefined;
      if (match !== null) {
        if (match[1] !== undefined) { description = match[1].trim(); }
        else { description = match[2].trim(); }
      }
      return {
        name: member.name.escapedText,
        ...(description !== undefined ? { description } : {}),
        type: typeChecker.typeToString(typeChecker.getTypeAtLocation(member.type)),
        ...(member.questionToken !== undefined ? { optional: true } : {}),
      }
    })
  };
}