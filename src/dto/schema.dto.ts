export type jsonType = string | number | boolean | null | { [key: string]: jsonType } | jsonType[];

export interface ISchemaDTO {
  name: string;
  description?: string;
  properties: IPropertyDTO[];
}

export interface IPropertyDTO {
  name: string;
  type: string;
  description?: string;
  optional?: boolean;
}